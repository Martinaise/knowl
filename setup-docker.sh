#!/bin/bash
##############################################################
## Knowl Bookshelf - System Setup (Dockerized Setup v0.2.3) ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): royalpatch,                                   ##
##############################################################

# Exit if there is an error
set -o errexit
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin

echo ""
echo "-----------------------------" | tee -a output.log
echo " Starting the Setup script.  " | tee -a output.log
echo "-----------------------------" | tee -a output.log
echo " Starting dts: " date
#Get the data that the script last ran
CURRENTDATE=`date +"%Y-%m-%d"`

echo "[010] Updating and downloading needed packages: "
sudo apt update # Not doing upgrade as I think that is unexpected behavior. If I run a script, I wouldn't want it to upgrade programs it doesn't use.
sudo apt install -y apt-transport-https wget gzip unzip unrar cifs-utils # docker-compose - I think there is a preferred method over apt install.

echo "[020] Setting up folders..."
mkdir -p knowl/db
cd knowl

echo "[030] Trying to download the sql files on:" $CURRENTDATE | tee -a output.log
echo "      The files will only be downloaded if newer." | tee -a output.log
echo "" | tee -a output.log

# [100] Use wget, log to the output.log. continue getting a partially downloaded
#     file, Only get files if the timestamp is newer.
#     Show progress bar on screen. Put the output in the db folder
echo "[101] Trying to download libgen_fiction_dbbackup-last.rar." | tee -a output.log
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll -P db http://185.39.10.101/dbdumps/fiction/libgen_fiction_dbbackup-last.rar | tee -a output.log

echo "[102] Trying to download libgen_all_dbbackup-last.rar." | tee -a output.log
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll -P db http://185.39.10.101/dbdumps/libgen/libgen_all_dbbackup-last.rar | tee -a output.log

echo "[103] Trying to download scimag.sql.gz." | tee -a output.log
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll -P db http://gen.lib.rus.ec/dbdumps/scimag.sql.gz | tee -a output.log

echo "[104] Trying to download CrossRef." | tee -a output.log
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll -P db -O crossref-works.zip https://ndownloader.figshare.com/files/17470100 | tee -a output.log


# [200] Once the files are downloaded, we will unpack the files.
#       Errors will be collected to the output.log
echo "[201] Trying to unrar libgen_fiction_dbbackup-last.rar." | tee -a output.log
mkdir -p docker/lf_init
unrar -ierr x -o+ db/libgen_fiction_dbbackup-last.rar ./docker/lf_init/ | tee -a output.log

echo "[202] Trying to unrar libgen_all_dbbackup-last.rar." >> output.log
mkdir -p docker/bw_init
unrar -ierr x -o+ db/libgen_all_dbbackup-last.rar ./docker/bw_init/ | tee -a output.log

echo "[203] Trying to unrar scimag.sql.gz." >> output.log
mkdir -p docker/sm_init
gzip -d db/scimag.sql.gz ./docker/sm_init/ | tee -a output.log

echo "[204] Trying to unzip crossref-works.zip." >> output.log
mkdir -p docker/cr_init
unzip db/crossref-works.zip ./docker/cr_init/ | tee -a output.log

# [300] Remove depricated features.
## 's/regexp/replacement/' Attempt to match regexp against the pattern space. If
##      successful, replace that portion matched with replacement. 
echo "[301] Removing depricated features on backup_libgen_fiction.sql" | tee -a output.log
sed -i 's/,NO_AUTO_CREATE_USER//' ./docker/lf_init/backup_libgen_fiction.sql | tee -a output.log

echo "[302] Removing depricated features on libgen.sql" | tee -a output.log
sed -i 's/,NO_AUTO_CREATE_USER//' ./docker/bw_init/libgen.sql | tee -a output.log

echo "[303] Removing depricated features on scimag.sql" | tee -a output.log
sed -i 's/,NO_AUTO_CREATE_USER//' ./docker/sm_init/scimag.sql | tee -a output.log

### [400] Here we will pull the newest docker-compose, .env file example,
###         and init scripts and put them all into the right folders.
###     If the files already exist, should move on
##
## Until then, will move on with assuming a docker-compose.yml is already in 
##      the folder where this script is being run
##

mkdir -p docker/logstash/drivers
echo "[401] Downloading the MySQL jdbc driver"
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll -P docker/logstash/drivers https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.19.zip | tee -a output.log

echo "[402] Trying to unrar the MySQL jdbc driver"
unrar -ierr x -o+ docker/logstash/drivers/mysql-connector-java-8.0.19.zip docker/logstash/drivers | tee -a output.log

############ COPY THE JAR TO THE CORRECT FOLDER OR ONLY PULL OUT THE JAR FROM THE RAR

echo "[410] Downloading files needed for DockerCompose."
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll https://gitlab.com/lucidhack/knowl/-/raw/master/docker/knowl-compose.yml | tee -a output.log
wget -a output.log -c -N --show-progress --progress=bar:force:noscroll https://gitlab.com/lucidhack/knowl/-/raw/master/docker/.env | tee -a output.log

####### Download the logstash files from gitlab.

# [500] Execute the docker-compose up command to setup the docker containers with the above sql files.
echo "[501] running docker compose" | tee -a output.log
docker-compose up -d | tee -a output.log
