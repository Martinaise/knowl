# Init file for the Bookwarrior MariaDB
# I am not entirely sure if this is needed....

# Exit if there are errors
set -o errexit

# CREATE THE MARIA DATABASE USER: bookuser PASSWORD: password
mysql <<ENDSQL
CREATE USER 'bookuser'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
ENDSQL

# GRANT ACCESS TO USER: bookuser
mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON bookwarrior . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL
