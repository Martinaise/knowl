# Docker Folder

This folder contains docker-compose files for setting up Knowl.

This will setup Knowl with as little input from the user as possible.

1. Download the docker-knowl folder using your preferred method.
2. Change any variables in the docker `.env` file if needed. It shouldn't be though.
3. Run `sh setup.sh` in the root folder.

#### That's it!
***
### Basic Steps performed: 
1. Downloads the libgen files if they are newer than what is already there.
2. Unrars the files.
3. Removed the depricated features from them
4. [not implemented yet] Pull newest compose and env files if not already present
5. Runs `docker-compose up -d` to bring all the containers up. 

At this point, you should be able to:
visit any database with: `docker exec -it <container-name> bash`
    <container-name> can be any of the containers made by docker-compose, including:
* bookwarrior_db (holds the bookwarrior database) _[MariaDB]_
* libfic_db (holds the libgen_fiction database) _[MariaDB]_

   For example: 
   ```
   $ docker exec -it knowl_bookwarrior_db bash
   $ mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD
   $ show databases;
   ```
The docker-compose also creates an 'adminer' instance which can be accessed, by default, on port 8080.
***
### What needs to be done:
* Pull knowl-compose files if one does not exist (don't write over one already there. Make a comment in the output.log if a newer version is available)
* Pull .env file if one does not exist (don't write over one already there)
* Continue with the Knowl setup past the database setup point. 
* At the end of the file, don't forget to clean-up! (e.g. Remove old .rar files)

- ~~Ubuntu 18.04.03 Server~~
- ~~MariaDB~~
- ~~Example Data~~
- Logstash
- Docker Compose; Elasticsearch + Kibana
- Elasticsearch Indices
- Kibana Queries
- Front-End - Conceptual Mockup
- Front-End - Development Effort(s)
- Content Indexing
- ML Automated Topic Classification

***
#### Dev setup
My current dev setup is:
* Windows 10 Pro Build 19559.rs_prerelease.200131-1437
* WSL2, Docker Desktop setup for WSL2, passing the socket
* WSL2 - Ubuntu 18.04.4

I run everything in the WSL2 instance. 

I code everything using VS Code Insiders. To do so, simply:
* Open VS Code
* Select 'Open Folder'
* In the address bar in the Explorer window, enter `\\wsl$\Ubuntu-18.04\home\<usernam>` where <username> is your WSL2 username. This will open your WSL2 Linux home folder in VS Code. 
* Set your default shell to your WSL2 instance.

Alternatively, install the 'Remote - WSL' extension in VS Code. ('Remote - WSL' runs commands and extensions directly in WSL so you don't have to worry about pathing issues, binary compatibility, or other cross-OS challenges. You're able to use VS Code in WSL just as you would from Windows.)

#####NOTE:
Using git with Remote-WSL can get odd. I don't have that setup. I use a different git application.