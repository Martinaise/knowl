import React, { Component } from 'react'
import extend from 'lodash/extend'
import { SearchkitManager,SearchkitProvider,
  SearchBox, Pagination, MenuFilter, TagCloud,
  HitsStats, SortingSelector, NoHits,
  ResetFilters, RangeFilter, CheckboxItemList,
  ViewSwitcherHits, ViewSwitcherToggle,
  InputFilter, GroupedSelectedFilters, PageSizeSelector,
  Layout, TopBar, LayoutBody, LayoutResults, Select,CheckboxFilter, TermQuery,
  ActionBar, ActionBarRow, SideBar } from 'searchkit'
import './index.css'
import Gravatar from 'react-gravatar'
import Icon from 'react-icons-kit';
import {questionCircle} from 'react-icons-kit/fa/questionCircle'
import {chainBroken} from 'react-icons-kit/fa/chainBroken'
import {arrowCircleODown} from 'react-icons-kit/fa/arrowCircleODown'

var url=""
var cover=""
var coverstyle = ""
var covertitle=""
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!==404;
}

const host = "http://3.21.107.135:9200/knowl*"
const searchkit = new SearchkitManager(host)


const ViewGridItem = (props)=> {
  const {bemBlocks, result} = props
  const source = extend({}, result._source, result.highlight)
  let url = "./covers/" + result._source.Cover
  let Filename = "./books/" + result._source.Filename
  let newFilename = result._source.Title + "." + result._source.Extension
  let gravsize=""
  url="./covers/" + result._source.Cover
  if(UrlExists(url)){
    coverstyle=""
    cover=url
    covertitle=""
    gravsize="0"
  }else{
    coverstyle = "wp-caption-frame"+ result._source.MD5.slice(-2,-1)
    cover="./coverless/skin/" + result._source.MD5.slice(-1) + ".jpg"
    covertitle=result._source.Title
    gravsize="50"
  }

  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} class="gridview" data-qa="hit">
      <a href={Filename} target="" download={newFilename}>
        <figure class="wp-caption">
          <img src={cover} alt="" data-qa="poster" width="246" height="380"/>
          <figcaption class={coverstyle}>{covertitle}<br/><br/></figcaption>
        </figure>
        <div class="gravatar"><Gravatar email={source.Title} size={gravsize} default="retro"/></div>
        {/* default can be set to "monsterid", "identicon", "wavatar", "retro", "robohash", "blank" */}
      </a>
    </div>
  )
}

const ViewDetailItem = (props)=> {
  const {bemBlocks, result} = props
  const source = extend({}, result._source, result.highlight)
  let Filename = "./books/" + result._source.Library + "/" + result._source.Filename
  let gravsize=""
  let booklink=""
  let linkstatus=""
  let bookalt=""
  url="./covers/" + result._source.Cover
  if(UrlExists(url)){
    coverstyle=""
    cover=url
    covertitle=""
    gravsize="0"
  }else{
    coverstyle = "wp-caption-frame" + result._source.MD5.slice(-2,-1)
    cover="./coverless/skin/" + result._source.MD5.slice(-1) + ".jpg"
    covertitle=result._source.Title
    gravsize="50"
  }
  if(UrlExists(Filename)){
    booklink=Filename
    bookalt="Local File Found! Click to download."
    linkstatus=arrowCircleODown
  }else{
    booklink="http://libgen.lc/foreignfiction/item/index.php?md5=" + result._source.MD5
    bookalt="Local File NOT Found!  Click for more options."
    linkstatus=chainBroken
  }
  return (
    <div data-qa="hit" class="gridview">

      <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
        <div className={bemBlocks.item("poster")}>
          <figure class="wp-caption">
            <img src={cover} alt="" data-qa="poster" width="246" height="380"/>
            <figcaption class={coverstyle}>{covertitle}<br/><br/></figcaption>
          </figure>
          <div class="gravatar"><Gravatar email={source.Title} size={gravsize}/></div>
        </div>
        <div className={bemBlocks.item("details")}>
          <span title={bookalt}><a href={booklink} target=""><Icon size={16} icon={linkstatus}/> <b dangerouslySetInnerHTML={{__html:source.Title}}/></a></span>
          <a href={url} target=""><div dangerouslySetInnerHTML={{__html:source.Tags}}/></a>

          <b>Description: </b><div dangerouslySetInnerHTML={{__html:source.Description}}></div>
          <div><hr/></div>
          <div className={bemBlocks.item("knowl-footer")}>
            <b>COVER -</b> Title: {source.Title}, Author(s): {source.Authors}, Publisher: {source.Publisher}, City: {source.City} Year: {source.Year}, Language: {source.Language}
            <br/><b>HASH -</b> AICH: {source.AICH}, CRC32: {source.CRC32}, MD5: {source.MD5}, SHA1: {source.SHA1}
            <br/><b>IDENTIFIER -</b> ID: {source.id}, DOI: {source.DOI}, ISBN: {source.ISBN}, ISSN: {source.ISSN}, UDC: {source.UDC}
            <br/><b>FILE -</b> Library: {source.Library}, PublicDomain: {source.PublicDomain}, Filesize: {source.Filesize}, Extension: {source.Extension}, Pages: {source.Pages}
            <br/><b>MISC -</b> {source.zero}
          </div>
        </div>

      </div>
    </div>
  )
}



class App extends Component {

    showSettings (event) {
      event.preventDefault();
    }


  render() {
    return (

    <SearchkitProvider searchkit={searchkit}>
      <Layout>

          <TopBar>
            <div className="my-logo"><a href="https://gitlab.com/lucidhack/knowl"><img alt="Knowl Bookshelf" src={"./navbar-logo.png"} width="188"/></a></div>
            <SearchBox
              prefixQueryFields={["Title^3", "Authors^2", "Description", "Tags"]}
              queryFields={["Title^3", "Authors^2", "Description", "Tags"]}
              placeholder="Search: Title, Author(s), Description and Tags"
              autofocus={true}
              searchOnChange={true}
              searchThrottleTime={5000}
              queryOptions={{analyzer:"standard"}}
             />
            <div style={{ color: '#BBBBBB' }}>&nbsp;&nbsp;<a style={{ color: '#BBBBBB' }} href="https://gitlab.com/lucidhack/knowl/-/wikis/User's-Guide"><Icon size={40} icon={questionCircle}/></a></div>
          </TopBar>

        <LayoutBody>

          <SideBar>
            <MenuFilter id="Libraries" title="Libraries" field="Library.keyword" listComponent={CheckboxItemList} />
            <CheckboxFilter id="PublicDomain" title="" label="PublicDomain" filter={TermQuery("PublicDomain", 'true')} /> <InputFilter id="Title" searchThrottleTime={2000} title="" placeholder="Title" searchOnChange={true} queryFields={["Title"]} />
            <InputFilter id="AuthorsPublisher" searchThrottleTime={5000} title="" placeholder="Author, Publisher" searchOnChange={true} queryFields={["Authors", "Publisher"]} />
            <InputFilter id="TagsDescription" searchThrottleTime={5000} title="" placeholder="Tags, Description" searchOnChange={true} queryFields={["Description","Tags"]} />
            <InputFilter id="Identifier" searchThrottleTime={5000} title="" placeholder="id, DOI, ISBN, ISSN, UDC" searchOnChange={true} queryFields={["id", "DOI","ISBN","ISSN","UDC"]} />
            <InputFilter id="Hash" searchThrottleTime={5000} title="" placeholder="AICH, CRC32, MD5, SHA1" searchOnChange={true} queryFields={["AICH","CRC32","MD5","SHA1"]} />
            <InputFilter id="Year" searchThrottleTime={5000} title="" placeholder="Year" searchOnChange={true} queryFields={["Year"]} />
            <RangeFilter id="YearRange" field="Year" min={0} max={2025} showHistogram={false} title=""/>
            <MenuFilter id="ProlificAuthor" field="Authors.keyword" title="Prolific Authors" listComponent={TagCloud} />
            <MenuFilter id="Extension" field="Extension.keyword" title="File Extensions" listComponent={TagCloud} />
            <MenuFilter id="Language" field="Language.keyword" title="Languages" listComponent={TagCloud} />
          </SideBar>

          <LayoutResults>
            <ActionBar>

              <ActionBarRow>
                Per Page:<PageSizeSelector options={[1,16]} listComponent={Select}/>&nbsp;&nbsp;&nbsp;&nbsp;
                View:<ViewSwitcherToggle listComponent={Select}/>&nbsp;&nbsp;&nbsp;&nbsp;
                Sort:<SortingSelector options={[
                  {label:"Relevance", field:"_score", order:"desc"},
                  {label:"ID", field:"id", order:"asc"},
                  {label:"Title", field:"Title.keyword", order:"asc"},
                  {label:"Authors", field:"Authors.keyword", order:"asc"},
                  {label:"Year (asc)", field:"Year", order:"asc"},
                  {label:"Year (desc)", field:"Year", order:"desc"},
                  {label:"CRC32", field:"CRC32.keyword", order:"asc"},
                ]}/>&nbsp;&nbsp;&nbsp;&nbsp;
                <b><HitsStats translations={{"hits6tats.results_found":"{hitCount} Results Found"}}/></b>

              </ActionBarRow>

              <ActionBarRow>
                <GroupedSelectedFilters/>
                <ResetFilters/>
              </ActionBarRow>

            </ActionBar>

            <ViewSwitcherHits
                hitsPerPage={16} highlightFields={["Title","Authors","Publisher","Language","Year","id","DOI","ISBN","ISSN","UDC","AICH","CRC32","MD5","SHA1","Description","Tags","Extension"]}
                sourceFilter={["Library", "id", "zero", "Visible", "Description", "Title", "Cover", "ISBN", "MD5", "AICH", "SHA1", "CRC32", "Year", "Language", "Publisher", "Extension","Tags","DOI","ISSN","UDC","Filename","Extension","Filesize","Pages", "PublicDomain"]}
                hitComponents={[
                  {key:"grid", title:"Grid", itemComponent:ViewGridItem},
                  {key:"list", title:"Detail", itemComponent:ViewDetailItem, defaultOption:true}
                ]}
                scrollTo="body"
            />
            <NoHits suggestionsField={"Title"}/>
            <Pagination showNumbers={true}/>

          </LayoutResults>

          </LayoutBody>
        </Layout>
      </SearchkitProvider>
    );
  }
}

export default App;


