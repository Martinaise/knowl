#!/bin/bash
##############################################################
## Knowl Bookshelf - System Setup (Bare-Metal Setup v0.1.6) ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack, royalpatch                         ##
##############################################################

PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin

echo "[010] # STARTING THE SETUP SCRIPT"
echo " Starting dts: " date

echo "[020] # INSTALL GENERAL SOFTWARE REQUIREMENTS"
sudo apt-get update -y
sudo apt-get install -y cifs-utils=2:6.8-1                         # For mounting remote ebook shares
sudo apt-get install -y mariadb-server=1:10.1.44-0ubuntu0.18.04.1  # To store our databases in
sudo apt-get install -y openjdk-8-jre=8u242-b08-0ubuntu3~18.04     # Required for ELK Stack
sudo apt-get install -y unrar=1:5.5.8-1                            # Extracting archives
sudo apt-get install -y zip=3.0-11build1                           # Extracting archives
sudo apt-get install -y unzip=6.0-21ubuntu1                        # Extracting archives
sudo apt-get install -y apt-transport-https=1.6.12                 # Required for Elasticsearch Repo
sudo apt-get install -y curl                                       # For checking Elasticsearch data

echo "[030] # INSTALL CORRECT YARN"
sudo apt-get -y remove cmdtest yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get  update -y
sudo apt-get install -y yarn

echo "[040] # ADD ELASTIC.CO REPOSITORY KEY"
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "# SAVE ELASTIC.CO REPOSITORY DEFINITION, AND UPDATE THE REPOSITORY"
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update

echo "[050] # INSTALL ELK STACK (Elasticsearch, Logstash, and Kibana)"
sudo apt-get install -y elasticsearch=7.6.0
sudo apt-get install -y kibana=7.6.0
sudo apt-get install -y logstash

echo "[060] # CONFIGURE ELASTICSEARCH"
# elasticsearch.yml configuration file
sudo rm -f /etc/elasticsearch/elasticsearch.yml.knowl
sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.knowl
sudo cp ~/knowl/bare-metal/elasticsearch/elasticsearch.yml /etc/elasticsearch
# jvm.options configuration file
sudo rm -f /etc/elasticsearch/jvm.options.knowl
sudo mv /etc/elasticsearch/jvm.options /etc/elasticsearch/jvm.options.knowl
sudo cp ~/knowl/bare-metal/elasticsearch/jvm.options /etc/elasticsearch
# DEV-NOTE: Need to get the following commands into elasticsearch.yml (or other file) to remain persistant
#ulimit -n 65536
#ulimit -u 2048

echo "[070] # CONFIGURE KIBANA"
# kibana.yml configuration file
sudo rm -f /etc/kibana/kibana.yml.knowl
sudo mv /etc/kibana/kibana.yml /etc/kibana/kibana.yml.knowl
sudo cp ~/knowl/bare-metal/kibana/kibana.yml /etc/kibana

echo "[080] # CONFIGURE LOGSTASH"
# jvm.options configuration file
sudo rm -f /etc/logstash/jvm.options.knowl
sudo mv /etc/logstash/jvm.options /etc/logstash/jvm.options.knowl
sudo cp ~/knowl/bare-metal/logstash/jvm.options /etc/logstash

echo "[090] # FETCH AND EXTRACT JDBC CONNECTOR"
# This allows communication between MariaDB and Logstash
cd /var/lib/logstash/
sudo wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.19.tar.gz
sudo tar -xvzf mysql-connector-java-8.0.19.tar.gz
sudo rm -f mysql-connector-java-8.0.19.tar.gz
sudo cp ./mysql-connector-java-8.0.19/mysql-connector-java-8.0.19.jar .
sudo rm -r mysql-connector-java-8.0.19

echo "[100] # (RE)START ELK STACK"
sudo systemctl restart elasticsearch
sudo systemctl start kibana
#sudo systemctl restart logstash

echo "[110] # ENABLE ELK STACK ON REBOOT"
sudo /bin/systemctl enable elasticsearch.service
sudo /bin/systemctl enable kibana.service
#sudo /bin/systemctl enable logstash.service

echo "[130] # OPEN FIREWALL PORTS"
sudo ufw allow 9200 # Elasticsearch
#sudo ufw allow 9300 # Elasticsearch
sudo ufw allow 5601 # Kibana

echo "[140] # CREATE THE PUBLIC-DOMAIN VIRTUAL LIBRARY"
sudo mysql -e "DROP DATABASE IF EXISTS public_domain;"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               sudo mysql -e "CREATE DATABASE public_domain;"                              # PUBLIC DOMAIN
sudo mysql -e "CREATE DATABASE public_domain;"
sudo mysql public_domain < ~/knowl/ingest/public_domain.sql

echo "[150] # CREATE THE MARIA DATABASE USER: bookuser PASSWORD: password"
sudo mysql <<ENDSQL
    CREATE USER 'bookuser'@'localhost' IDENTIFIED BY 'password';
    FLUSH PRIVILEGES;
ENDSQL

echo "[160] # GRANT bookuser ACCESS TO public_domain"
sudo mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON public_domain . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL

echo "[170] # DOWNLOAD KNOWL BOOKSHELF DEPENDENCIES"
cd ~/knowl/bookshelf
yarn
yarn add searchkit --save
yarn add react-icons-kit --save
yarn add react-gravatar --save

echo "[180] # START KNOWL BOOKSHELF"
echo "Knowl Bookshelf will default to http://localhost:3000"
cd ~/knowl/bookshelf
yarn start
