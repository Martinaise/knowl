#!/bin/bash
##############################################################
## Knowl Bookshelf - Library Setup (articles.sh) v0.0.1     ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f scimag.sql.gz

# Fetch the articles database from your favorite mirror 
wget https://lgmirror.simon987.net/scimag_2020-02-29.sql.gz -O scimag.sql.gz
#wget http://gen.lib.rus.ec/dbdumps/scimag.sql.gz

# Unpack the database
gzip -d -v scimag.sql.gz

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' ~/knowl/ingest/scimag.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen_scimag;"
sudo mysql -e "CREATE DATABASE libgen_scimag;"

# Import our databse into MariaDB
sudo mysql libgen_scimag < scimag.sql
rm -f scimag.sql

# Grant user "bookuser" access
sudo mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON libgen_scimag . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL

# Run logstash, to create an Elasticsearch index from the MariaDB database
sudo rm -f /etc/logstash/conf.d/*.conf
sudo cp ~/knowl/conf.d/libgen-scimag.conf /etc/logstash/conf.d
sudo -Hu logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f /etc/logstash/conf.d/
sudo rm -f /etc/logstash/conf.d/*.conf
