#!/bin/bash
##############################################################
## Knowl Bookshelf - Library Setup (science.sh) v0.0.1      ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f libgen.rar

# Fetch the science database from your favorite mirror 
wget https://lgmirror.simon987.net/libgen_2020-03-05.rar -O libgen.rar
#wget http://185.39.10.101/dbdumps/libgen/libgen_all_dbbackup-last.rar -O libgen.rar
#wget http://libgen.lc/dbdumps/libgen/libgen_all_dbbackup-last.rar -O libgen.rar
#wget http://gen.lib.rus.ec/dbdumps/libgen.rar

# Unpack the database
unrar x libgen.rar
rm -f libgen.rar

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' libgen.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS bookwarrior;"
sudo mysql -e "CREATE DATABASE bookwarrior;"

# Import our databse into MariaDB
sudo mysql bookwarrior < libgen.sql
rm -f libgen.sql

# Grant user "bookuser" access
sudo mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON bookwarrior . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL

# Run logstash, to create an Elasticsearch index from the MariaDB database
sudo rm -f /etc/logstash/conf.d/*.conf
sudo cp ~/knowl/conf.d/libgen-science.conf /etc/logstash/conf.d
sudo -Hu logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f /etc/logstash/conf.d/
sudo rm -f /etc/logstash/conf.d/*.conf