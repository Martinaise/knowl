#!/bin/bash
##############################################################
## Knowl Bookshelf - Library Setup (fiction.sh) v0.0.1      ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Change to our ingest folder, and purge last/old file
cd ~/knowl/ingest/
rm -f backup_libgen_fiction.rar

# Fetch the science database from your favorite mirror 
# The default dropbox libgen.rar is a smaller public-domain version, useful for quick testing/eval of Knowl Bookshelf.
#wget http://185.39.10.101/dbdumps/fiction/libgen_fiction_dbbackup-last.rar -O backup_libgen_fiction.rar
wget http://libgen.lc/dbdumps/fiction/libgen_fiction_dbbackup-last.rar -O backup_libgen_fiction.rar
#wget http://gen.lib.rus.ec/dbdumps/fiction.rar -O backup_libgen_fiction.rar

# Unpack the database
unrar x backup_libgen_fiction.rar
rm -f backup_libgen_fiction.rar

# Remove NO_AUTO_CREATE_USER
sed -i 's/,NO_AUTO_CREATE_USER//' backup_libgen_fiction.sql

# Recreate a blank database
sudo mysql -e "DROP DATABASE IF EXISTS libgen_fiction;"
sudo mysql -e "CREATE DATABASE libgen_fiction;"

# Import our databse into MariaDB
sudo mysql libgen_fiction < backup_libgen_fiction.sql
rm -f backup_libgen_fiction.sql

# Grant user "bookuser" access
sudo mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON libgen_fiction . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL

# Run logstash, to create an Elasticsearch index from the MariaDB database
sudo rm -f /etc/logstash/conf.d/*.conf
sudo cp ~/knowl/conf.d/libgen-fiction.conf /etc/logstash/conf.d
sudo -Hu logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f /etc/logstash/conf.d/
sudo rm -f /etc/logstash/conf.d/*.conf
