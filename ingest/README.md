# Ingest Folder

The ingest folder is where the setup-bare-metal.sh script will dowload databases to.  This working/staging directory is where the databases will then be unpacked and then loaded into MariaDB.

# Public-Domain Example/Test Databases

## Objective

Provide Public-Domain *"friendly"* versions of the Libgen booklists.  This smaller subset provides a highly compact subset of data that can be used for quick install/evaluation of the Knowl Bookshelf.  Due to the gitlab file-size restriction, the smaller public-domain test versions of these databases are hosted on a 2GB Drop-Box account <a href="https://www.dropbox.com/sh/4wngcey6uyidsf3/AACgOsodIiKXe0konvldAD6oa?dl=0">***HERE***</a>.

By default, bare-metal-setup.sh will dowload the smaller public-domain databases from Drop-Box to get Knowl Bookshelf up and running quickly.  If you prefer to use larger and/or more up-to-date versions of libgen, make sure to modify the <a href="../bare-metal/">wget download locations</a> in science.sh, fiction.sh, and articles.sh `before` you run <a href="../setup-bare-metal.sh">../setup-bare-metal.sh</a>

* Preview <a href="https://www.dropbox.com/s/kjfyv2owslq8h9j/public_domain.sql?dl=0">public_domain.sql</a>

## How the public-domain databases were created

First, a public_domain.sql reference database was created, derived from the Gutenberg Project's GUTINDEX.ALL file.  Entries that were flagged as copyrighted (denoted with a 'C' after the books id number) were removed.  Additionally, other entries that caused issues with the trasfer to public_domain.sql were also excluded (mostly some foreign works with characters I didn't know how to deal with).  The resulting public_domain.sql database is a two column table (ptitle, and pauthor) with each record containing an author name and a book title.

Next, the public_domain.sql file was joined to each of the libgen databases.  If an author and title match was made, the record was kept (the others were dropped).  While this may not be 100% robust, it does create much smaller versions of the libgen databases that are likely to be *nearly/mostly* all public domain works.  

Future improvements:  The validity of the gutenberg copyright flags were not cross-checked or verified.  If works were misflagged in the Gutenberg GUTINDEX.ALL file, those errors would manifest here as well.  Also, a simple author and title match is not super robust.  More granular matching (i.e. ISBN, Year, Publisher, etc) would produce more accurate/realiable results.  

### Creating libgen_all_dbbackup-last.rar (Science)

* Cull bookwarrior Mariadb Database records down to a *mostly/probably* public-domain version
* Pre-requisites: public_domain.sql and bookwarrior.sql need to both be loaded into mariadb
* Warning: this process will destructively modify your MariaDB bookwarrior database.  Make sure you have a backup on disk.
* Yields approximatly 1.3K records

MariaDB Commands:
```bash
# DROP TABLES THAT ARE NOT USED BY KNOWL BOOKSHELF
# FOR PUBLIC DOMAIN VERSION TO SAVE SPACE
use bookwarrior;
DROP TABLE IF EXISTS bookwarrior.updated_edited;
DROP TABLE IF EXISTS bookwarrior.description_edited;

# DROP COPYRIGHT RECORDS FROM bookwarrior.updated
DROP TABLE IF EXISTS bookwarrior.tmp_updated;
CREATE TABLE bookwarrior.tmp_updated SELECT * FROM bookwarrior.updated LIMIT 0;
ALTER TABLE tmp_updated ADD COLUMN (ptitle TEXT, pauthor TEXT);
INSERT INTO tmp_updated SELECT * FROM bookwarrior.updated LEFT JOIN public_domain.works ON bookwarrior.updated.author=public_domain.works.pauthor WHERE bookwarrior.updated.title=public_domain.works.ptitle;
ALTER TABLE tmp_updated DROP COLUMN ptitle;
ALTER TABLE tmp_updated DROP COLUMN pauthor;
DROP TABLE IF EXISTS bookwarrior.updated;
CREATE TABLE bookwarrior.updated SELECT * FROM bookwarrior.tmp_updated;
DROP TABLE IF EXISTS bookwarrior.tmp_updated;

# DROP COPYRIGHT WORKS FROM bookwarrior.description
DROP TABLE IF EXISTS bookwarrior.tmp_description;
CREATE TABLE bookwarrior.tmp_description SELECT * FROM bookwarrior.description LIMIT 0;
INSERT INTO tmp_description (id, md5, descr, toc, TimeLastModified) SELECT bookwarrior.description.id, bookwarrior.description.md5, bookwarrior.description.descr, bookwarrior.description.toc, bookwarrior.description.TimeLastModified FROM bookwarrior.description LEFT JOIN bookwarrior.updated ON bookwarrior.description.id=bookwarrior.updated.id WHERE bookwarrior.description.id=bookwarrior.updated.id;
DROP TABLE IF EXISTS bookwarrior.description;
CREATE TABLE bookwarrior.description SELECT * FROM bookwarrior.tmp_description;
DROP TABLE IF EXISTS bookwarrior.tmp_description;
```

Shell Commands:
```bash
# DUMP THE NEW PUBLIC-DOMAIN VERSION OF BOOKWARRIOR TO DISK
sudo mysqldump bookwarrior > libgen.sql
rar a libgen_all_dbbackup-last.rar libgen.sql 
```

### Creating libgen_fiction_dbbackup-last.rar (Fiction)

* Cull libgen_fiction_dbbackup-last.rar Mariadb Database records down to a *mostly/probably* public-domain version
* Pre-requisites: public_domain.sql and libgen_fiction.sql need to both be loaded into mariadb
* Warning: this process will destructively modify your MariaDB libgen_fiction database.  Make sure you have a backup on disk.
* Yields approximatly 56K records

*(untested)* MariaDB Commands:
```bash
# DROP COPYRIGHT RECORDS FROM libgen_fiction.main
DROP TABLE IF EXISTS libgen_fiction.tmp_main;
CREATE TABLE libgen_fiction.tmp_main SELECT * FROM libgen_fiction.main LIMIT 0;
ALTER TABLE libgen_fiction.tmp_main ADD COLUMN (ptitle TEXT, pauthor TEXT);

INSERT INTO libgen_fiction.tmp_main SELECT * FROM libgen_fiction.main LEFT JOIN public_domain.works ON main.title=works.ptitle WHERE public_domain.works.pauthor LIKE main.AuthorFamily1;
ALTER TABLE tmp_updated DROP COLUMN ptitle;
ALTER TABLE tmp_updated DROP COLUMN pauthor;
DROP TABLE IF EXISTS libgen_fiction.main;
CREATE TABLE libgen_fiction.main SELECT * FROM libgen_fiction.tmp_main;
DROP TABLE IF EXISTS libgen_fiction.tmp_main;

# DROP COPYRIGHT WORKS FROM libgen_fiction.hashes
DROP TABLE IF EXISTS libgen_fiction.tmp_hashes;
CREATE TABLE libgen_fiction.tmp_hashes SELECT * FROM libgen_fiction.hashes LIMIT 0;
INSERT INTO tmp_hashes (md5, crc32, edonkey, aich, sha1, tth, torrent, btih, sha256) SELECT libgen_fiction.hashes.md5, libgen_fiction.hashes.crc32, libgen_fiction.hashes.edonkey, libgen_fiction.hashes.aich, libgen_fiction.hashes.sha1, libgen_fiction.hashes.tth, libgen_fiction.hashes.torrent, libgen_fiction.hashes.btih, libgen_fiction.hashes.sha256 FROM libgen_fiction.hashes LEFT JOIN libgen_fiction.main ON libgen_fiction.hashes.md5=blibgen_fiction.main;
DROP TABLE IF EXISTS libgen_fiction.hashes;
CREATE TABLE libgen_fiction.hashes SELECT * FROM libgen_fiction.tmp_hashes;
DROP TABLE IF EXISTS libgen_fiction.tmp_hashes;
```

Shell Commands:
```bash
# DUMP THE NEW PUBLIC-DOMAIN VERSION OF LIBGEN_FICTION TO DISK
sudo mysqldump libgen_fiction > backup_libgen_fiction.sql
rar a libgen_fiction_dbbackup-last.rar backup_libgen_fiction.sql
```

### Creating scimag.sql.gz (Articles)

* Cull scimag.sql.gz Mariadb Database records down to the first 1K records.
* Pre-requisites: libgen_scimag needs to be loaded into mariadb
* Warning: this process will destructively modify your MariaDB libgen_scimag database.  Make sure you have a backup on disk.
* Yields 1K records

MariaDB Commands:
```bash
# DROP TABLES THAT ARE NOT USED BY KNOWL BOOKSHELF TO SAVE SPACE
use libgen_scimag;
DROP TABLE IF EXISTS libgen_scimag.error_report;
DROP TABLE IF EXISTS libgen_scimag.publishers;

# KEEP ONLY THE FIRST 1K RECORDS FROM libgen_scimag
DROP TABLE IF EXISTS libgen_scimag.tmp_scimag;
CREATE TABLE libgen_scimag.tmp_scimag SELECT * FROM libgen_scimag.scimag WHERE ID<=1000;
DROP TABLE IF EXISTS libgen_scimag.scimag;
CREATE TABLE libgen_scimag.scimag SELECT * FROM libgen_scimag.tmp_scimag;
DROP TABLE IF EXISTS libgen_scimag.tmp_scimag;

# KEEP RECORDS IN libgen_scimag.magazines THAT HAS ID<1000
DROP TABLE IF EXISTS libgen_scimag.tmp_magazines;
CREATE TABLE libgen_scimag.tmp_magazines SELECT * FROM libgen_scimag.magazines WHERE libgen_scimag.magazines.id<1000;
DROP TABLE IF EXISTS libgen_scimag.magazines;
CREATE TABLE libgen_scimag.magazines SELECT * FROM libgen_scimag.tmp_magazines;
DROP TABLE IF EXISTS libgen_scimag.tmp_magazines;
```

Shell Commands:
```bash
# DUMP THE TRUNCATED LIBGEN_SCIMAG TO DISK
sudo mysqldump libgen_scimag > scimag.sql
gzip scimag.sql
```
