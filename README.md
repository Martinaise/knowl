<img src="logo512.png" width="250">

## Overview

The ***Knowl Bookshelf*** aims to be a unified front-end for all of your ebook databases.  Often, there may be significant structural differences between various ebook sources (i.e. Project Gutenberg Project, Internet Archive, Open Library, etc). ***Knowl Bookshelf*** mutates these differing sources into a common set of univeral Knowl fields. This consolidated/unified database is then presented to the end user as an elegant Single-Page Application (SPA). 

If you are looking for books (pdf, epub, etc) you are in the wrong place.  This page in only for the organization and retrieval of your existing book lists.  If you want to download some free books, I highly recommend you visit https://www.gutenberg.org/.  However, you do not need to own a single eBook to work through this project.

## Objective

Setup a search engine for eBooks.  Search technology will be based on Elasticsearch, Logstash, and Kibana (collectively referred to as the “ELK Stack”).  We also have some advanced feautes planned on our <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Roadmap-Features">***Features Roadmap***</a> page.    

We will be using Libgen book listings as our example data. However, with some modification, these techniques should also work for other publicly available book lists.  The reader is encouraged to expand these techniques to additional datasets, and share their logstash *.config files (submit a merge request for your *.config to the conf.d folder

## Live Demo

View a live demo of the <a href="http://3.21.107.135:3000/?PublicDomain=true&Title=Alice%20Wonderland&TagsDescription=rabbit">***Knowl Bookshelf***</a>.

## Screenshots

Detail View (default): 

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/250e4fe95657baff6c6006ba25c1347b/detail-view.png" width="800">

Grid View:

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/e5a99b3d8c087759d609bca0ccc2ed02/grid-view.png" width="800">

## Procedural Book-Cover Generation

If a book-cover does not exist on disk, one will be generated on the fly. There are 36 different texture backgrounds, and 36 label styles.  Combined, this allows for 1,296 different book-styles that will be automagically applied to books with no covers. A book-style (texture and label) is each determined at run-time by one character (each) in the MD5 hash.  This ensures that the same book-style cover is applied across different systems.  There is also an Identicon graphic on the book-cover, which is derived from the books Title.  

<img src="https://gitlab.com/lucidhack/knowl/-/wikis/uploads/0641b1ad227c677377d17b99b4ad62e8/random-cover-generator.png" width="800">

## Installation

Three installation options are planned to suit your preference and/or requirement; ***Manual***, ***Bare-metal***, and ***Docker***.  These options are exclusive, and you only need to choose one of these options to implement ***Knowl Bookshelf*** on your system.

<b>OPTION 1 - Manual Setup</b>
  * This walks you through the installation, and explains in sections what the install scripts normally do.
  * This option is recomended for anyone who wants a better understanding of the install process, or for anyone wishing to have greater control over the installation process.
  * For manual setup <a href="https://gitlab.com/lucidhack/knowl/-/wikis/home"><b>Proceed to the Wiki Homepage</b></a>

<b>OPTION 2 - Bare-Metal Setup</b> *(No Virtualization)*
  
1. Proceed to your chosen terminal
2. Enter `git clone https://gitlab.com/lucidhack/knowl.git -b dev-beta-0.2.0 ~/knowl`
3. Manually edit two files, and specify the ip address of the target system. <a href="https://gitlab.com/lucidhack/knowl/issues/53">See issue #53</a>
   * On line 99 of ~/knowl/bare-metal/elasticsearch/elasticsearch.yml, change the two occurances of `localhost` to your ***IP address***.
   * On line 27 of ~/knowl/bookshelf/src/App.js, change the occurances of `0.0.0.0` to your ***IP address***.
4. Then `sh -x ~/knowl/setup-bare-metal.sh >> ~/knowl/setup-bare-metal.out`
5. Sit back and wait for it to finish.
6. Fetch and process the desired libraries by running at *least* one of the following commands.
   * `sh ~/knowl/bare-metal/science.sh`
   * `sh ~/knowl/bare-metal/fiction.sh`
   * `sh ~/knowl/bare-metal/articles.sh`
7. Using the Kibana interface, you need to manually create a ***knowl**** search index pattern.  This only needs to be done once, see ***Create an index pattern*** section <a href="https://gitlab.com/lucidhack/knowl/-/wikis/Query-eBook-Data">***here***</a> for more details.
8. Access knowl at http://yourIPaddress:3000

Note:  If you restart your server, you will need to manually re-launch the bookshelf app by running the following commands:
```bash
# LAUNCH KNOWL BOOKSHELF APP (defaults to port 3000)
cd ~/knowl/bookshelf
yarn start
```

<b>OPTION 3 - Docker Setup</b> *(Virtual Docker Containers)* NOTE: This method is currently under development.

1. Proceed to your chosen terminal.
2. Enter `curl https://gitlab.com/lucidhack/knowl/-/raw/master/setup-docker.sh?inline=false | sh`
3. Sit back and wait for it to finish.
