#!/bin/bash
##############################################################
## Knowl Bookshelf - System Setup (Bare-Metal Setup v0.1.6) ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack, royalpatch                         ##
##############################################################

PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
KNOWL_DIR=`pwd`

echo "# STARTING THE SETUP SCRIPT"
echo " Starting dts: " date

echo "# INSTALL GENERAL SOFTWARE REQUIREMENTS"

sudo pacman -Syy --noconfirm
sudo pacman -S --noconfirm cifs-utils
sudo pacman -S --noconfirm mariadb
sudo pacman -S --noconfirm jdk8-openjdk
sudo pacman -S --noconfirm unrar
sudo pacman -S --noconfirm zip
sudo pacman -S --noconfirm curl                                  # For checking Elasticsearch data

echo "# INSTALL CORRECT YARN"
sudo pacman -S --noconfirm yarn

echo "# INSTALL ELK STACK (Elasticsearch, Logstash, and Kibana)"
sudo pacman -S --noconfirm elasticsearch
sudo pacman -S --noconfirm kibana
sudo pacman -S --noconfirm logstash

echo "# CONFIGURE ELASTICSEARCH"
# elasticsearch.yml configuration file
sudo rm -f /etc/elasticsearch/elasticsearch.yml.knowl
sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.knowl
sudo cp $KNOWL_DIR/bare-metal/elasticsearch/elasticsearch.yml /etc/elasticsearch
# jvm.options configuration file
sudo rm -f /etc/elasticsearch/jvm.options.knowl
sudo mv /etc/elasticsearch/jvm.options /etc/elasticsearch/jvm.options.knowl
sudo cp $KNOWL_DIR/bare-metal/elasticsearch/jvm.options /etc/elasticsearch
# DEV-NOTE: Need to get the following commands into elasticsearch.yml (or other file) to remain persistant
#ulimit -n 65536
#ulimit -u 2048

echo "# CONFIGURE KIBANA"
# kibana.yml configuration file
sudo rm -f /etc/kibana/kibana.yml.knowl
sudo mv /etc/kibana/kibana.yml /etc/kibana/kibana.yml.knowl
sudo cp $KNOWL_DIR/bare-metal/kibana/kibana.yml /etc/kibana

echo "# CONFIGURE LOGSTASH"
# jvm.options configuration file
sudo rm -f /etc/logstash/jvm.options.knowl
sudo mv /etc/logstash/jvm.options /etc/logstash/jvm.options.knowl
sudo cp $KNOWL_DIR/bare-metal/logstash/jvm.options /etc/logstash

echo "# FETCH AND EXTRACT JDBC CONNECTOR"
# This allows communication between MariaDB and Logstash
cd /var/lib/logstash/
sudo wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.19.tar.gz
sudo tar -xvzf mysql-connector-java-8.0.19.tar.gz
sudo rm -f mysql-connector-java-8.0.19.tar.gz
sudo cp ./mysql-connector-java-8.0.19/mysql-connector-java-8.0.19.jar .
sudo rm -r mysql-connector-java-8.0.19

echo "# (RE)START ELK STACK"
sudo systemctl restart elasticsearch
sudo systemctl start kibana

echo "# ENABLE ELK STACK ON REBOOT"
sudo systemctl enable elasticsearch.service
sudo systemctl enable kibana.service

echo "# CREATE THE PUBLIC-DOMAIN VIRTUAL LIBRARY"
sudo mysql -e "DROP DATABASE IF EXISTS public_domain;"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               sudo mysql -e "CREATE DATABASE public_domain;"                              # PUBLIC DOMAIN
sudo mysql -e "CREATE DATABASE public_domain;"
sudo mysql public_domain < $KNOWL_DIR/ingest/public_domain.sql

echo "# CREATE THE MARIA DATABASE USER: bookuser PASSWORD: password"
sudo mysql <<ENDSQL
    CREATE USER 'bookuser'@'localhost' IDENTIFIED BY 'password';
    FLUSH PRIVILEGES;
ENDSQL

echo "# GRANT bookuser ACCESS TO public_domain"
sudo mysql <<ENDSQL
    GRANT USAGE ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
    GRANT SELECT, LOCK TABLES ON public_domain . * TO 'bookuser'@'localhost';
    FLUSH PRIVILEGES;
ENDSQL

echo "# DOWNLOAD KNOWL BOOKSHELF DEPENDENCIES"
cd $KNOWL_DIR/bookshelf
sudo yarn
sudo yarn add searchkit --save
sudo yarn add react-icons-kit --save
sudo yarn add react-gravatar --save

echo "# START KNOWL BOOKSHELF"
echo "Knowl Bookshelf will default to http://localhost:3000"
cd $KNOWL_DIR/bookshelf
sudo yarn start